package INICIANTE.Objetos.Projetos;

public class Carro {
    public static final String VERMELHO = "vermelho";
    public static final String PRETO = "preto";
    public static final String PRATA = "prata";
    public static final String BRANCO = "branco";
    public static final String Arcondicionado = null;

    private Integer QuantidadePneus;
    private Integer QuantidadeCalotas;
    private Integer QuantidadeParafusosPneu;
    private String cor;
    private Integer NumeroChassi;
    private Integer anoFabricação;
    private Integer combustivel;


    public Carro(Integer QuantidadePneus) {
        setQuantidadePneus(QuantidadePneus);
    }

    public Integer getanoFabricação() {
        return anoFabricação;
    }

    public Integer getcombustivel() {
        return combustivel;
    }
    public Integer getQuantidadePneus() {
        return QuantidadePneus + 1;
    }

    public Integer getQuantidadeCalotas() {
        return QuantidadeCalotas;
    }

    public Integer getNumeroChassi() {
        return NumeroChassi;
    }

    public Integer getQuantidadeParafusoPneu() {
        return QuantidadeParafusosPneu;
    }

    public void setQuantidadeCalotas(Integer QuantidadeCalotas) {
        this.QuantidadeCalotas = QuantidadeCalotas;
    }

    public void setQuantidadePneus(Integer QuantidadePneus) {
        setQuantidadeCalotas(QuantidadePneus);
        QuantidadeParafusosPneu = QuantidadePneus * 4;
        this.QuantidadePneus = QuantidadePneus;
    }

    public void setQuantidadeParafusoPneu(Integer QuantidadeParafusoPneu) {
        this.QuantidadeParafusosPneu = QuantidadeParafusoPneu;
    }

    public void setNumeroChassi(Integer NumeroChassi) {
        this.NumeroChassi = NumeroChassi;
    }

    public void setanoFabricação(Integer anoFabricação) {
        this.anoFabricação = anoFabricação;
    }

    public void setcombustivel(Integer combustivel) {
        this.combustivel = combustivel;
    }
    public String getCor(){
        return cor;
    }
    
    public void setCor(String cor) {
        this.cor = cor;
    }
    

    public void imprimeValores() {
        System.out.println("Quantidade Pneus" + getQuantidadePneus());
        System.out.println("Quantidade de Calotas" + getQuantidadeCalotas());
        System.out.println("Quantidade Parafuso Pneu" + getQuantidadeParafusoPneu());
        System.out.println("Cor " + getCor());
        System.out.println("Número Chassi " + getNumeroChassi());
        System.out.println("Ano de Fabricação" + getanoFabricação());
        System.out.println("Combustivel" + getcombustivel());


    }


    
}

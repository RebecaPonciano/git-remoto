package Modulo7;

public class TestaConta {
    public static void main(String[] args, int agencia){
        ContaCorrente cc1 = new ContaCorrente(1,1,"bancoA",100.00,1000.00);
        System.out.println(cc1);

        System.out.println("O saldo da Conta Corrente é R$ "+cc1.getSaldo());

        ContaPoupanca p1 = new ContaPoupanca(33,3,"banco C",10.00,20,0.05);
        System.out.println("O saldo da Conta Poupança é R$ "+p1.getSaldo());

        ContaSalario s1 = new ContaSalario(33, 3,"banco D", 10.000);
        System.out.println("O saldo da Conta Salário é R$ "+s1.getSaldo());   
    }


}

package Modulo7;

public class ContaCorrente extends Conta {
    private double chequeEspecial;

    public ContaCorrente(int numero, int agencia, String banco, double saldo, double chequeEspecial) {
        super(numero, agencia, banco, saldo);
        this.chequeEspecial = chequeEspecial;
    }
    @Override
    public String toString() {
        return "ContaCorrente:  " +
                "chequeEspecial = " + chequeEspecial +
                '}';
    }

    public void sacar(Double valor) {
        if(valor > this.getSaldo()) {
            System.out.println("Saldo indisponivel para o valor de saque");
        }
        else {
            this.saldo-=valor;
        }               
    }

    public void depositar(Double valor) {
        this.saldo+=valor;
    }

    public double getSaldo() {
        return (this.chequeEspecial + this.saldo);
        

     }



}

